'use strict';

var app = angular.module("myApp.home",["ngRoute","firebase","myApp.user"])
.config(["$routeProvider",function($routeProvider){
    
    $routeProvider.when("/home",{
        controller: "expoCtrl",
        templateUrl:"pages/home/home.html"
    });
    
}]);

