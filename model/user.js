'use strict';

var app = angular.module("myApp.user",["ngRoute","firebase"])
.config(["$routeProvider",function($routeProvider){
    
    $routeProvider.when("/user",{
        controller: "expoCtrl",
        templateUrl:"pages/user/dashboard.html"
    });
    
}]);

app.controller("expoCtrl",function($scope,$firebaseArray){
    
    var ref = new Firebase("https://angularpruevaapp.firebaseio.com/expos");
    
    $scope.expos = $firebaseArray(ref);
    
    $scope.agregar = function(){
     
       $scope.expos.$add({         
            descripcion: $scope.descripcion,
            estado: $scope.estado,
            imagen: $scope.imagen,
            titulo: $scope.titulo
       });
      
 
        $scope.titulo = ''
        $scope.imagen = ''
        $scope.estado = ''
        $scope.descripcion = ''
    };  
});