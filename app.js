'use stritc';

angular
.module('myApp',["ngRoute","myApp.user","myApp.home"])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/user'});
}]);