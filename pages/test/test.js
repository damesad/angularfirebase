'use strict';

angular.module("myApp.test",["ngRoute"])
.config(["$routeProvider",function($routeProvider){
    
    $routeProvider.when("/test",{
        controller: "holactrl",
        templateUrl:"pages/test/holamundo.html"
    });
    
}])

.controller("holactrl",["$scope",function($scope){
    $scope.saludo="Hola mundo"
}]);